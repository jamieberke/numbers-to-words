import "./scss/styles.scss";

var numberInput;
var conversionResult = document.getElementsByClassName(
  "number-to-words__result"
)[0];
var conversionInput = document.getElementsByClassName(
  "number-to-words__input"
)[0];
var units = [
  "",
  "one",
  "two",
  "three",
  "four",
  "five",
  "six",
  "seven",
  "eight",
  "nine",
  "ten",
  "eleven",
  "twelve",
  "thirteen",
  "fourteen",
  "fiveteen",
  "sixteen",
  "seventeen",
  "eighteen",
  "nineteen",
];
var tens = [
  "",
  "twenty",
  "thirty",
  "fourty",
  "fifty",
  "sixty",
  "seventy",
  "eighty",
  "ninety",
];
var triples = ["", "thousand", "millon", "billion", "trillion"];

class NumberToWordsDemo {
  constructor() {
    this.bindEvents();
  }

  bindEvents() {
    conversionInput.addEventListener("keyup", (e) => {
      numberInput = conversionInput.value;
      this.parseNumberInput(numberInput);
    });
    conversionInput.addEventListener("input", (e) => {
      numberInput = conversionInput.value;
      this.parseNumberInput(numberInput);
    });
  }

  parseNumberInput(numberInput) {
    var number = parseInt(numberInput);
    if (number < 0) {
      conversionResult.innerHTML = `minus ${this.doNumberConvert(number * -1, 0)}`;
    } else if (number === 0) {
      conversionResult.innerHTML = "zero";
    } else if (number > 0) {
      conversionResult.innerHTML = this.doNumberConvert(parseInt(number), 0);
    } else {
      conversionResult.innerHTML = "";
    }
  }

  generateHundreds(y, numberString) {
    if (y > 0) {
      return `${numberString + units[y]} hundred `
    } else {
      return "";
    }
  }

  generateTriples(recursion, numberString) {
    return `${numberString} ${triples[recursion]}`;
  }

  generateTensUnits(z, numberString) {
    if (z < 20) {
      return `${numberString}${units[z]}`;
    } else {
      return `${numberString}${tens[parseInt(z / 10) - 1]} ${units[z % 10]}`;
    }
  }

  doNumberConvert(num, recursion) {
    // Triples modifier - reduces our number for each iteration of doNumberConvert() so that we can
    // add triples modifiers (generateTriples) to the string if there is more string to generate.
    // To do this we check if the generated number is above 1 when divided by 1000 which will indicate there
    // are more hundreds/tens/units to handle
    var x = Math.floor(num / 1000);
    // Get hundreds remainder
    var y = Math.floor(num / 100) % 10;
    // Get tens/units remainders
    var z = num % 100;
    var numberString = "";

    // Work out hundreds
    numberString = this.generateHundreds(y, numberString);

    // Work out units and tens
    numberString = this.generateTensUnits(z, numberString);

    // Add triples modifier for larger numbers (thousands, million etc) only if the recursion step is at
    // one or greater as this would indicate the number is a number in its thousands or greater
    if (recursion >= 1) {
      numberString = this.generateTriples(recursion, numberString);
    }

    // Recurse if there is still a remainder as we still have more number string to generate
    if (x > 0)
      return `${this.doNumberConvert(x, recursion + 1)} ${numberString.trim()}`;
    else {
      return numberString.trim();
    }
  }
}

new NumberToWordsDemo();
