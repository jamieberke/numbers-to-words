# Number To Words Converter

#### Description
This component demonstrates a conversion of a integer into words. For example, `25` to `twenty five`. At each iteration of the conversion the base number is reduced down so that we can work out the `units/tens/hundreds` for each triplet. Each triplet represents a stage in the number conversion such as `units/tens/hundreds`, `thousands`, `millions`, `billions` etc, where the triplets modifier is applied where applicable. The triplets modifier is the part of the string which modifies the `units/tens/hundreds` to make them `thousands`, `millions` etc (e.g. two hundred `thousand` or thirty `million`).

#### How to use the demo

1. Access the project route
2. Run `npm install`
2. Run `npm run start`